public class Card {
    private int rank;
    private String suit;

    public Card(int rank, String suit) {
        this.rank = rank;
        this.suit = suit;
    }

    //Getter for rank
    public int getRank() {
        return rank;
    }

    //Setter for rank. Private for now
    public void setRank(int rank) {
        this.rank = rank;
    }

    //Getter for suit
    public String getSuit() {
        return suit;
    }

    //Setter for suit. Private for now
    public void setSuit(String suit) {
        this.suit = suit;
    }

    //Returns object into string
    public String toString() {
        return rank + " of " + suit;
    }

    //Returns score depending on card
    public double calculateScore() {
        double score = 0.0;

        switch (suit) {
            case "Hearts":
                score = this.rank + 0.4;
                break;
            
            case "Spades":
                score = this.rank + 0.3;
                break;
            case "Diamonds":
                score = this.rank + 0.2;
                break;

            case "Clubs":
                score = this.rank + 0.1;
                break;

            default:
                break;
        }

        return score;
    }
}
