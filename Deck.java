import java.util.Random;
import java.util.Arrays;
public class Deck {
    private int cardAmount;
    private Card[] stack;
    private Random random;

    //initialize deck of 52 cards. 4 suits, 13 ranks.
    public Deck(int cardAmount) {
        this.cardAmount = cardAmount;
        this.stack = new Card[this.cardAmount];

        //have two arrays representing values for suits and ranks
        String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
        int[] ranks = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

        int index = 0;

        //assign number 1-13 to each suit, representing each card in the deck 
        for (int i = 0; i < suits.length; i++) {
            for (int j = 0; j < ranks.length; j++) {
                stack[index] = new Card(ranks[j], suits[i]);
                index++;
            }
        }
    }

    //returns length of deck
    public int length() {
        return stack.length;
    }
    
    //returns and removes top card of deck
    public Card getTopCard() {
        Card drawnCrard = stack[0];
        this.stack = removeTopCard();

        return drawnCrard;
    }
    
    //removes top card of deck
    private Card[] removeTopCard() {
        Card[] newDeck = new Card[this.stack.length - 1];
        int i = 1;

        for (int index = 0; index < newDeck.length; index++) {
            newDeck[index] = this.stack[i];
            i++;
        }

        return newDeck;
    } 

    //shuffles cards in deck
    public void shuffleDeck() {
        Random random = new Random();

        //loop through each index to swap cards
        for (int i = 0; i < this.stack.length; i++) {
            int randomIndex = random.nextInt(this.stack.length);
            
            //clone card at random index
            Card temporaryCard = this.stack[randomIndex];

            //Swap cloned card with card at index 'i'
            this.stack[randomIndex] = this.stack[i];
            this.stack[i] = temporaryCard;
        }
    }

    //returns string of whole deck
    public String toString() {
        StringBuilder sb = new StringBuilder();
    
        for (int i = 0; i < this.stack.length; i++) {
            sb.append(stack[i].toString()+ "\n");
        }
    
        return sb.toString();
    }
}