import java.util.Scanner;

public class SimpleWar {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        int cardAmount = 52;
        Deck deck = new Deck(cardAmount);
        deck.shuffleDeck();

        System.out.println("Press ENTER to start the game");
        reader.nextLine();

        System.out.print("\033[H\033[2J"); //Clear terminal
        System.out.flush();

        //Intialize round number
        int roundNumber = 1;
        
        //Initialize player points
        int playerOnePoints = 0;
        int playerTwoPoints = 0;

        //Game starts here
        while (deck.length() > 0) {

            System.out.println("Round " + roundNumber + ".");
            roundNumber++;

            Card playerOneCard = deck.getTopCard();
            Card playerTwoCard = deck.getTopCard();

            System.out.println("-------------------------------");
            System.out.println("Player one card: " + playerOneCard);
            System.out.println("Player two card: " + playerTwoCard);
            System.out.println("-------------------------------");

            if (playerOneCard.calculateScore() > playerTwoCard.calculateScore()) {
                System.out.println("Player one gets this round.");
                playerOnePoints += 1;
            } else {
                System.out.println("Player two gets this round");
                playerTwoPoints += 1;
            }

            System.out.println(); //Skip a line
            System.out.println("Player one has " + playerOnePoints + " points");
            System.out.print("Player two has " + playerTwoPoints + " points");

            System.out.println("\n\nPress ENTER to continue");
            reader.nextLine();
        } 

        System.out.print("\033[H\033[2J"); //Clear terminal
        System.out.flush();

        if (playerOnePoints > playerTwoPoints)
            System.out.printf("Player ONE wins with %d points!", playerOnePoints);
        else 
        if (playerTwoPoints > playerOnePoints)
            System.out.printf("Player TWO wins with %d points!", playerTwoPoints);
        else
            System.out.println("Tie!");

        reader.close();
    }
}
